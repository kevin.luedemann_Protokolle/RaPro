\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}
\contentsline {section}{\numberline {2}Theorie}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Kernkr\IeC {\"a}fte und das Atom}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Zerfall}{1}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}$\alpha $-Zerfall}{2}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}$\beta $-Zerfall}{2}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}$\gamma $-Zerfall}{2}{subsubsection.2.2.3}
\contentsline {subsection}{\numberline {2.3}Aktivierung von Silber}{2}{subsection.2.3}
\contentsline {section}{\numberline {3}Durchf\IeC {\"u}hrung}{3}{section.3}
\contentsline {section}{\numberline {4}Auswertung}{4}{section.4}
\contentsline {section}{\numberline {5}Diskussion}{11}{section.5}
\contentsline {subsection}{\numberline {5.1}Halbwertszeiten}{11}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Aktivit\IeC {\"a}ten}{12}{subsection.5.2}
\contentsline {section}{\nonumberline Literatur}{13}{section*.15}
